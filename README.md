This is actually a repackaged subset of [t1/test-tools](https://github.com/t1/test-tools),
which doesn't play nicely with Gradle, as in Gradle you can't specify dependencies that are
visible at compile and test time but not at runtime, so I had to split the annotations into a
separate module... which is cleaner anyway.

I will probably remove it from `test-tools` and continue here.
