package com.github.t1.packagedependenciestest;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target(PACKAGE)
public @interface DependsOn {
    Class<?>[] packagesOf() default {};
}
