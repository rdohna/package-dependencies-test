package com.github.t1.packagedependenciestest;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

class DependenciesFinder {
    private String root;

    DependenciesFinder(String root) { this.root = root; }

    Map<String, Set<String>> read() {
        TreeMap<String, Set<String>> out = new TreeMap<>();
        try (ScanResult scan = new ClassGraph()
            .verbose()
            .enableAllInfo()
            .enableExternalClasses()
            .enableInterClassDependencies()
            .whitelistPackages(root)
            .scan()) {
            for (ClassInfo classInfo : scan.getAllClasses()) {
                if (classInfo.isExternalClass())
                    continue;
                Set<String> dependencies = classInfo.getClassDependencies().stream()
                    .map(ClassInfo::getPackageName)
                    .filter(pkg -> !pkg.equals(classInfo.getPackageName()))
                    .collect(Collectors.toSet());
                out.computeIfAbsent(classInfo.getPackageName(), n -> new TreeSet<>())
                    .addAll(dependencies);
            }
        }
        return out;
    }
}
