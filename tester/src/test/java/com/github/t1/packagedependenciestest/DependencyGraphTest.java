package com.github.t1.packagedependenciestest;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Path;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static com.github.t1.packagedependenciestest.AbstractPackageDependenciesTest.DEPENDENCIES_DOT;
import static com.github.t1.packagedependenciestest.AbstractPackageDependenciesTest.packageDependencies;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class DependencyGraphTest {
    private DependencyBuilder givenDependency(String source) { return new DependencyBuilder(source); }

    public static class DependencyBuilder {
        private final String source;

        DependencyBuilder(String source) { this.source = source; }

        void on(String... targets) { packageDependencies.put(source, asSet(targets)); }
    }

    private static Set<String> asSet(String... elements) { return new TreeSet<>(asList(elements)); }

    @SuppressWarnings("SameParameterValue") private static String contentOf(Path path) { return Assertions.contentOf(path.toFile()); }

    private final AbstractPackageDependenciesTest test = new AbstractPackageDependenciesTest() {};

    @Before public void setUp() { packageDependencies = new TreeMap<>(); }

    @After public void tearDown() { packageDependencies.clear(); }

    @Test public void shouldProduceEmptyGraph() {
        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "}\n");
    }

    @Test public void shouldProduceGraphWithThreeNodes() {
        givenDependency("a").on("b", "c");
        givenDependency("b").on("");
        givenDependency("c").on("");

        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "    a -> b;\n"
            + "    a -> c;\n"
            + "}\n");
    }

    @Test public void shouldProduceGraphWithCommonPackage() {
        givenDependency("p.a").on("p.b", "p.c");
        givenDependency("p.b").on("");
        givenDependency("p.c").on("d");

        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "    p_a [label=\"a\"];\n"
            + "    p_b [label=\"b\"];\n"
            + "    p_c [label=\"c\"];\n"
            + "\n"
            + "    p_a -> p_b;\n"
            + "    p_a -> p_c;\n"
            + "}\n");
    }

    @Test public void shouldProduceGraphWithCommonPackageStartingWithParent() {
        givenDependency("p.q").on("p.q.a", "p.q.b");
        givenDependency("p.q.a").on("");
        givenDependency("p.q.b").on("c");

        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "    p_q [label=\"q\"];\n"
            + "    p_q_a [label=\"a\"];\n"
            + "    p_q_b [label=\"b\"];\n"
            + "\n"
            + "    p_q -> p_q_a;\n"
            + "    p_q -> p_q_b;\n"
            + "}\n");
    }

    @Test public void shouldProduceGraphWithSuffixMatchingPackage() {
        givenDependency("p.q.a").on("p.q.b", "p.qx.c");
        givenDependency("p.q.b").on("");
        givenDependency("p.qx.c").on("d");

        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "    subgraph cluster_q {\n"
            + "        graph [label=\"q\"];\n"
            + "        p_q_a [label=\"a\"];\n"
            + "        p_q_b [label=\"b\"];\n"
            + "    }\n"
            + "    subgraph cluster_qx {\n"
            + "        graph [label=\"qx\"];\n"
            + "        p_qx_c [label=\"c\"];\n"
            + "    }\n"
            + "\n"
            + "    p_q_a -> p_q_b;\n"
            + "    p_q_a -> p_qx_c;\n"
            + "}\n");
    }

    @Test public void shouldProduceGraphWithPackageClusters() {
        givenDependency("p.q.a").on("p.q.b", "p.r.a");
        givenDependency("p.q.b").on("x", "y");
        givenDependency("p.r.a").on("p.r.b");
        givenDependency("p.r.b").on("");

        test.shouldProduceDotFile();

        assertThat(contentOf(DEPENDENCIES_DOT)).isEqualTo(""
            + "strict digraph {\n"
            + "    node [shape=box];\n"
            + "\n"
            + "    subgraph cluster_q {\n"
            + "        graph [label=\"q\"];\n"
            + "        p_q_a [label=\"a\"];\n"
            + "        p_q_b [label=\"b\"];\n"
            + "    }\n"
            + "    subgraph cluster_r {\n"
            + "        graph [label=\"r\"];\n"
            + "        p_r_a [label=\"a\"];\n"
            + "        p_r_b [label=\"b\"];\n"
            + "    }\n"
            + "\n"
            + "    p_q_a -> p_q_b;\n"
            + "    p_q_a -> p_r_a;\n"
            + "    p_r_a -> p_r_b;\n"
            + "}\n");
    }
}
