@DependsOn(packagesOf = {
    com.github.t1.graph.Graph.class,
    com.github.t1.packagedependenciestest.AbstractPackageDependenciesTest.class,
    org.junit.Test.class,
})
package test;

import com.github.t1.packagedependenciestest.DependsOn;
